#!/usr/bin/env python
import os
import sys
import yaml
#import subprocess
#from subprocess import run
# import shlex
from os import listdir


##VARIABLES
## directorio home
homedir=os.environ['HOME']

## saco el directorio de entornos
environmentsfolder=homedir+"/devops/environments/"

valores=['aws_access_key_id', 'aws_secret_access_key' ]




## LOGICA
if  (len(sys.argv) <= 1):
    for environments in listdir (homedir+"/devops/environments"):
        print environments
elif (sys.argv[1] == 'shortlist'):
    for environments in listdir (homedir+"/devops/environments"):
        print environments         
else:        
    entorno=sys.argv[1]
    settingsfile=entorno.lower()+".yml"
    with open(environmentsfolder+entorno+"/"+settingsfile) as fp:
        my_configuration = yaml.load(fp)
    print "export ENVALIAS="+entorno.upper()
    print "export KUBECONFIG=$HOME/devops/environments/$ENVALIAS/namespaces/kubeconfig"
    for valor in valores:    
        print "export "+valor.upper()+"="+my_configuration['env'].get(valor.lower(),"noexist")+"\n"